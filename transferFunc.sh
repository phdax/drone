#!/bin/bash

# $1 : DroneID
# $2 : A URL of target log file
# $3 : SCP Destination directory

suffix=`date "+%Y%m%d%H%M%S"`
logname="$1"_"$suffix".log
zipname="$1"_"$suffix".zip

curl $2 -o $logname
zip $zipname $logname

LOG=/opt/drone/runtime/script.log

# transfer
echo [`date "+%Y-%m-%d %H:%M:%S"`] "Send $zipname to $3 ..." >> $LOG 2>&1
  echo "Sending to Cloud." > /opt/drone/transferstat.staus
scp $zipname $3
scp_status=$?
echo scp status $? >> $LOG


# first retry  
if [ $scp_status != 0 ]; then
  echo [`date "+%Y-%m-%d %H:%M:%S"`] "Retry to send $zipname after 5s sleep ..." >> $LOG 2>&1
  echo "Retry 5s sleep" > /opt/drone/transferstat.staus
  sleep 5s
  scp $zipname $3
  scp_status=$?
fi

# second retry
if [ $scp_status != 0 ]; then
  echo [`date "+%Y-%m-%d %H:%M:%S"`] "Retry to send $zipname after 10s sleep ..." >> $LOG 2>&1
  echo "Retry 10s sleep" > /opt/drone/transferstat.staus
  sleep 10s
  scp $zipname $3
  scp_status=$?
fi

echo result $logname $zipname >> $LOG

# result
rm -f ./$logname
rm -f ./$zipname
if [ $scp_status = 0 ]; then
  echo [`date "+%Y-%m-%d %H:%M:%S"`] "Finished to send $zipname." >> $LOG 2>&1
  echo "Finished to send" > /opt/drone/transferstat.staus
  exit 0
else
  echo [`date "+%Y-%m-%d %H:%M:%S"`] "failed to send $zipname." >> $LOG 2>&1
  echo "Failed to send" > /opt/drone/transferstat.staus
  exit 1
fi
