#!/bin/bash

LOG=/opt/drone/runtime/script.log

#有線LANの起動
sudo ifup eth0

#GPIOの初期化
sudo echo 17 > /sys/class/gpio/export
sudo echo in > /sys/class/gpio/gpio17/direction
sudo echo high > /sys/class/gpio/gpio17/direction

#--------------------------------------------------

echo [`date "+%Y-%m-%d %H:%M:%S"`] "Task launcher start." >> $LOG 2>&1

landstat="0"

while :
do


   #本体画面表示
   if [ "`/usr/bin/pgrep -fo status.py`" = "" ]; then 
   sh /opt/drone/status.sh &
   fi

   #着地スイッチが入っていれば
   if [ "`cat /sys/class/gpio/gpio17/value`" = "1" ]; then
   
     if [ "$landstat" = "0" ]; then
     echo [`date "+%Y-%m-%d %H:%M:%S"`] "Status: Landing" >> $LOG 2>&1
     landstat="1"
     
    #SORACOMが無通信状態の場合開始
      if [ "`/sbin/ifconfig | grep ppp0 | wc -l`" = "0" ]; then
   	 echo [`date "+%Y-%m-%d %H:%M:%S"`] "SORACOM Connection Start" >> $LOG 2>&1
   	 sudo /etc/init.d/soracomair start
      fi

      fi

     #データチェック＆転送
      if [ "`/usr/bin/pgrep -fo logTransferTask.sh`" = "" ]; then 
      /opt/drone/logTransferTask.sh &
      fi

   #着地スイッチが入っていない場合
   else

     if [ "$landstat" = "1" ]; then
       echo [`date "+%Y-%m-%d %H:%M:%S"`] "Status; Flight" >> $LOG 2>&1
       landstat="0"
     
     #SORACOMが通信状態の場合停止
     if [ "`/sbin/ifconfig | grep ppp0 | wc -l`" = "1" ]; then
   	echo [`date "+%Y-%m-%d %H:%M:%S"`] "SORACOM Connection Stop" >> $LOG 2>&1
   	sudo /etc/init.d/soracomair stop
     fi

     fi


   
   fi
   sleep 10s
done

echo [`date "+%Y-%m-%d %H:%M:%S"`] "Task launcher end." >> $LOG 2>&1

