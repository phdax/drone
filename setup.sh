#! /bin/bash

cp /opt/maglab/env.sh /opt/drone

. /opt/maglab/env.sh

# flashair
sudo rm -f /etc/wpa_supplicant/wpa_supplicant.conf
echo 'country=JP' | sudo tee -a /etc/wpa_supplicant/wpa_supplicant.conf
echo 'ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev' | sudo tee -a /etc/wpa_supplicant/wpa_supplicant.conf
echo 'update_config=1' | sudo tee -a  /etc/wpa_supplicant/wpa_supplicant.conf
sudo wpa_passphrase "$FLASHAIR_SSID" "$FLASHAIR_PASS" | sudo tee -a /etc/wpa_supplicant/wpa_supplicant.conf
sudo chown root:root /etc/wpa_supplicant/wpa_supplicant.conf
sudo chmod 600 /etc/wpa_supplicant/wpa_supplicant.conf
sudo wpa_cli reconfigure
sudo service networking restart

# setup script
chmod 755 /opt/drone/landingcheck.sh
chmod 755 /opt/drone/launcher.sh
chmod 755 /opt/drone/logTransferTask.sh
chmod 755 /opt/drone/status.py
chmod 755 /opt/drone/status.sh
chmod 755 /opt/drone/transferFunc.sh

# cron
crontab /opt/drone/cron.conf

# i2c
sudo cp /dev/null /etc/modules
echo i2c-bcm2708  | sudo tee -a /etc/modules
echo i2c-dev  | sudo tee -a /etc/modules

# hostname
sudo cp /dev/null /etc/hostname
sudo cp /dev/null /etc/hosts
echo $DRONE_ID | sudo tee -a /etc/hostname
echo "127.0.0.1 localhost" | sudo tee -a /etc/hosts
echo "127.0.0.1 $DRONE_ID" | sudo tee -a /etc/hosts
sudo hostname

# ssh key
echo $SSH_ID_RSA_PUB > ~/.ssh/id_rsa.pub
echo $SSH_ID_RSA > ~/.ssh/id_rsa
echo "StrictHostKeyChecking no" > $HOME/.ssh/config
chmod 600 ~/.ssh/id_rsa
