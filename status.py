#!/usr/bin/python
# coding: shift_jis

import math
import time
import datetime
import RPi.GPIO as GPIO
import os
import commands

import Adafruit_GPIO.SPI as SPI
import Adafruit_SSD1306

from modemcmd import modemcmd
from modemcmd import ModemcmdException
from modemcmd import ModemcmdTimeoutException

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

RST = 24
disp = Adafruit_SSD1306.SSD1306_128_64(rst=RST)
disp.begin()
width = disp.width
height = disp.height
disp.clear()
disp.display()
image = Image.new('1', (width, height))
draw = ImageDraw.Draw(image)
SENSOR_PIN = 25
GPIO.setmode(GPIO.BCM)
GPIO.setup(SENSOR_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(RST ,GPIO.OUT)



while True:

  pin_status = GPIO.input(SENSOR_PIN)
#  print pin_status
#  time.sleep(1)
  while pin_status==1:

#            time.sleep(0.2)
            pin_status = GPIO.input(SENSOR_PIN)
#            print pin_status

            font = ImageFont.truetype('/opt/drone/font/OpenSans-Light.ttf', 15, encoding='shift_jis')
#            rssi = commands.getoutput ('echo "at+csq"  > /dev/ttyUSB1 ; cat /dev/ttyUSB1 | grep CSQ')
#            print rssi

            try:
                result = modemcmd('/dev/ttyUSB1', 'AT+CSQ', 10)
                online_chk = commands.getoutput ('/sbin/ifconfig | grep ppp0 | wc -l')
                stat1 = commands.getoutput ('/bin/cat /opt/drone/transferstat.staus')
                file_cloud = commands.getoutput ('/bin/cat /opt/drone/CURRENT_FILE.TXT')
                file_drone = commands.getoutput ('/bin/cat /opt/drone/NEW_FILE.TXT')
                print result
            except ModemcmdTimeoutException as e:
                print e
            except ModemcmdException as e:
                print e

            print "3:" + result
            result = result.split()
            print result

            if result[0]=='+CSQ:':
                spc=1
            elif result[1]=='+CSQ:':
                spc=2
            elif result[2]=='+CSQ:':
                spc=3

            result = result[spc].split(",")

            print result
            if int(result[0]) == 99:
                rssi_bar="xxxx"

            elif int(result[0]) == 0:
                rssi_bar="----"

            elif int(result[0]) == 1:
                rssi_bar="o---"

            elif 16 > int(result[0]) >= 2:
                rssi_bar="oo--"

            elif 31 > int(result[0]) >= 16:
                rssi_bar="ooo-"

            elif int(result[0]) >=31:
                rssi_bar="oooo"


            if int(online_chk) == 1:
                online_stat = "ONLINE"

            elif int(online_chk) == 0:
                online_stat = "OFFLINE"


            print rssi_bar + " RSSI score:" + result[0] + ":" +online_stat
            rssi_str = rssi_bar + ":" + result[0] + " / " + online_stat
            maxwidth, unused = draw.textsize(rssi_str, font=font)
            draw.rectangle((0,0,width,height), outline=0, fill=0)
            x=0
            for i, c in enumerate(rssi_str):
                if x > width:
                    break
                if x < -20:
                    char_width, char_height = draw.textsize(c, font=font)
                    x += char_width
                    continue
                y = 0
                draw.text((x, y), c, font=font, fill=255)
                char_width, char_height = draw.textsize(c, font=font)
                x += char_width


            font = ImageFont.truetype('/opt/drone/font/OpenSans-Light.ttf', 15, encoding='unic')
            time_str = stat1
            maxwidth, unused = draw.textsize(time_str, font=font)
            x=2
            for i, c in enumerate(time_str):
                if x > width:
                    break
                if x < -20:
                    char_width, char_height = draw.textsize(c, font=font)
                    x += char_width
                    continue
                y = 15
                draw.text((x, y), c, font=font, fill=255)
                char_width, char_height = draw.textsize(c, font=font)
                x += char_width


            font = ImageFont.truetype('/opt/drone/font/OpenSans-Light.ttf', 15, encoding='unic')
            print "Drone:" + file_drone + " / Cloud:" + file_cloud
            time_str = "DRN:" + file_drone[22:24] + " / CLD:" + file_cloud[22:24]
            maxwidth, unused = draw.textsize(time_str, font=font)
            x=2
            for i, c in enumerate(time_str):
                if x > width:
                    break
                if x < -20:
                    char_width, char_height = draw.textsize(c, font=font)
                    x += char_width
                    continue
                y = 32
                draw.text((x, y), c, font=font, fill=255)
                char_width, char_height = draw.textsize(c, font=font)
                x += char_width


            font = ImageFont.truetype('/opt/drone/font/OpenSans-Light.ttf', 15, encoding='unic')
            time_str = "Stat time:" + datetime.datetime.now().strftime("%H:%M:%S")
            maxwidth, unused = draw.textsize(time_str, font=font)
            x=2
            for i, c in enumerate(time_str):
                if x > width:
                    break
                if x < -20:
                    char_width, char_height = draw.textsize(c, font=font)
                    x += char_width
                    continue
                y = 48
                draw.text((x, y), c, font=font, fill=255)
                char_width, char_height = draw.textsize(c, font=font)
                x += char_width

            disp.image(image)
            disp.display()

            if pin_status==0:
                disp.clear()
                disp.display()