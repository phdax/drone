#!/bin/bash

cd `dirname $0`

. ./env.sh

#echo [`date "+%Y-%m-%d %H:%M:%S"`] "Task start." >> $LOG  2>&1

UPLOAD_DEST=${UPLOAD_DEST_USER}@${UPLOAD_DEST_HOST}:${UPLOAD_DEST_DIR}

# confirm onnection to FlashAir
curl -LI ${FLASHAIR_BASEURL}${NEW_F} -f -s -o /dev/null
if [ $? != 0 ]; then
  echo [`date "+%Y-%m-%d %H:%M:%S"`] "connection to FlashAir refused." >> $LOG  2>&1
  echo "FA Error" > /opt/drone/transferstat.staus
  exit 1
fi
# On the first time, unconditionally transfer NEW_FILE
if [ ! -e $CURRENT_F ]; then
  curl -s -O ${FLASHAIR_BASEURL}${NEW_F}
  cp $NEW_F $CURRENT_F
  next_file=`cat  $NEW_F | rev | cut -c 1-12 | rev`
  echo [`date "+%Y-%m-%d %H:%M:%S"`] "DOWNLOAD $next_file" >> $LOG 2>&1
  sh transferFunc.sh $DRONE_ID ${FLASHAIR_BASEURL}${next_file} $UPLOAD_DEST
  if [ $? = 1 ]; then
    echo [`date "+%Y-%m-%d %H:%M:%S"`] "Task failed." >> $LOG 2>&1
    exit 1
  fi
  echo [`date "+%Y-%m-%d %H:%M:%S"`] "Task initialized." >> $LOG 2>&1
  exit 0
fi

# Confirm the newest log name from FlashAir
curl -s -O ${FLASHAIR_BASEURL}${NEW_F}
newest=`cat ${NEW_F} | awk -F'/' '{ print $NF }'`
current=`cat ${CURRENT_F} | awk -F'/' '{ print $NF }'`
#echo [`date "+%Y-%m-%d %H:%M:%S"`] "CURRENT: $current <-> NEW: $newest" >> $LOG 2>&1
if [ $newest = $current ]; then
#  echo [`date "+%Y-%m-%d %H:%M:%S"`] "Nothing to do." >> $LOG 2>&1
  exit 0
fi

newest_num=`echo $newest | rev | cut -c 5-12 | rev`
current_num=`echo $current | rev | cut -c 5-12 | rev` 
diff=`expr $newest_num - $current_num`

# Not rotated
if [ $diff -gt 0 ]; then
  next_num=`expr $current_num + 1`
  for i in `seq $next_num $newest_num`
  do
    next_file=`printf "%08d.LOG" $i`
    echo [`date "+%Y-%m-%d %H:%M:%S"`] "DOWNLOAD $next_file" >> $LOG 2>&1
    sh transferFunc.sh $DRONE_ID ${FLASHAIR_BASEURL}${next_file} $UPLOAD_DEST
    if [ $? = 1 ]; then
      echo [`date "+%Y-%m-%d %H:%M:%S"`] "Task failed." >> $LOG 2>&1
      
      exit 1
    fi
  done

# Rotated
elif [ $diff -lt 0 ]; then

  # Find the rotation bound
  i=`expr $current_num + 1`
  i_file=`printf "%08d.LOG" $i`
  curl -LI ${FLASHAIR_BASEURL}${i_file} -f -s -o /dev/null
  while [ $? -eq 0 ]
  do
    i=`expr $i + 1`
    i_file=`printf "%08d.LOG" $i`
    curl -LI ${FLASHAIR_BASEURL}${i_file} -f -s -o /dev/null
  done
  bound_num=`expr $i - 1`

  # Download logs
  next_num=`expr $current_num + 1`
  for i in `seq $next_num $bound_num`
  do
    next_file=`printf "%08d.LOG" $i`
    echo [`date "+%Y-%m-%d %H:%M:%S"`] "DOWNLOAD $nextfile" >> $LOG 2>&1
    if [ "`/usr/bin/pgrep -fo transferFunc.sh`" = "" ]; then
    sh transferFunc.sh $DRONE_ID ${FLASHAIR_BASEURL}${next_file} $UPLOAD_DEST
    fi
    if [ $? = 1 ]; then
      echo [`date "+%Y-%m-%d %H:%M:%S"`] "Task failed." >> $LOG 2>&1
      echo "Task failed" > /opt/drone/transferstat.staus
      exit 1
    fi
  done
  for i in `seq 1 $newest_num`
  do
    next_file=`printf "%08d.LOG" $i`
    echo [`date "+%Y-%m-%d %H:%M:%S"`] "DOWNLOAD $nextfile" >> $LOG 2>&1
    if [ "`/usr/bin/pgrep -fo transferFunc.sh`" = "" ]; then
    sh transferFunc.sh $DRONE_ID ${FLASHAIR_BASEURL}${next_file} $UPLOAD_DEST
    echo "Get log from FA" > /opt/drone/transferstat.staus
    fi
    if [ $? = 1 ]; then
      echo [`date "+%Y-%m-%d %H:%M:%S"`] "Task failed." >> $LOG 2>&1
      exit 1
    fi
  done

fi

rm -f ./${CURRENT_F}
mv ./${NEW_F} ./${CURRENT_F}

echo [`date "+%Y-%m-%d %H:%M:%S"`] "Task finished." >> $LOG 2>&1
echo "Finish FA Transfer" > /opt/drone/transferstat.staus


